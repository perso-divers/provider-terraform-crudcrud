terraform {
  required_providers {
    beeceptor = {
      source = "hashicorp.com/edu/beeceptor"
    }
  }
}

provider "beeceptor" {
  database = "cacd8518f496affdc195"
}

resource "beeceptor_livre" "livres" {
  items = [
    {
      author = "Author1"
      nom = "Nom de livre 2"
    },
        {
      author = "Author2"
      nom = "Nom de livre 2"
    }
  ]
}


resource "beeceptor_livre" "livres2" {
  items = [
    {
      author = "Author3"
      nom = "Nom de livre 3"
    },
        {
      author = "Author4"
      nom = "Nom de livre 4"
    }
  ]
}
