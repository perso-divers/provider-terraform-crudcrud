package provider

import (
	"context"
	clientBeeceptor "terraform-provider-beeceptor/internal/provider/client"

	"github.com/hashicorp/terraform-plugin-framework/datasource"
	"github.com/hashicorp/terraform-plugin-framework/path"
	"github.com/hashicorp/terraform-plugin-framework/provider"
	"github.com/hashicorp/terraform-plugin-framework/provider/schema"
	"github.com/hashicorp/terraform-plugin-framework/resource"
	"github.com/hashicorp/terraform-plugin-framework/types"
)

// Ensure the implementation satisfies the expected interfaces.
var (
	_ provider.Provider = &beeceptorProvider{}
)

// New is a helper function to simplify provider server and testing implementation.
func New(version string) func() provider.Provider {
	return func() provider.Provider {
		return &beeceptorProvider{
			version: version,
		}
	}
}

// beeceptorProvider is the provider implementation.
type beeceptorProvider struct {
	// version is set to the provider version on release, "dev" when the
	// provider is built and ran locally, and "test" when running acceptance
	// testing.
	version string
}

// DataSources defines the data sources implemented in the provider.
func (p *beeceptorProvider) DataSources(_ context.Context) []func() datasource.DataSource {
	return nil
}

// Metadata returns the provider type name.
func (p *beeceptorProvider) Metadata(_ context.Context, _ provider.MetadataRequest, resp *provider.MetadataResponse) {
	resp.TypeName = "beeceptor"
	resp.Version = p.version
}

// Schema defines the provider-level schema for configuration data.
func (p *beeceptorProvider) Schema(_ context.Context, _ provider.SchemaRequest, resp *provider.SchemaResponse) {
	resp.Schema = schema.Schema{
		Attributes: map[string]schema.Attribute{
			"database": schema.StringAttribute{
				Optional: true,
			},
		},
	}
}

func (p *beeceptorProvider) Configure(ctx context.Context, req provider.ConfigureRequest, resp *provider.ConfigureResponse) {

	// Retrieve provider data from configuration
	var config beeceptorProviderModel
	diags := req.Config.Get(ctx, &config)
	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}

	// If practitioner provided a configuration value for any of the
	// attributes, it must be a known value.

	if config.Database.IsUnknown() {
		resp.Diagnostics.AddAttributeError(
			path.Root("host"),
			"Missing beeceptor database value",
			"Missing beeceptor database value",
		)
	}

	database := config.Database.ValueString()

	client, err := clientBeeceptor.NewClient(database)

	if err != nil {
		resp.Diagnostics.AddError(
			"Unable to Create beeceptor API Client",
			"An unexpected error occurred when creating the beeceptor API client. "+
				"If the error is not clear, please contact the provider developers.\n\n"+
				"beeceptor Client Error: "+err.Error(),
		)
		return
	}

	// Make the beeceptor client available during DataSource and Resource
	// type Configure methods.
	resp.DataSourceData = client
	resp.ResourceData = client
}

// Resources defines the resources implemented in the provider.
func (p *beeceptorProvider) Resources(_ context.Context) []func() resource.Resource {
	return []func() resource.Resource{
		NewLivreResource,
	}
}

// beeceptorProviderModel maps provider schema data to a Go type.
type beeceptorProviderModel struct {
	Database types.String `tfsdk:"database"`
}
