package clientBeeceptor

type Livre struct {
	Id     string `json:"id"`
	Author string `json:"author"`
	Nom    string `json:"nom"`
}
