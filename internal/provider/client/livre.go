package clientBeeceptor

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
)

// GetLivre - Returns a specifc livre
func (c *Client) GetLivre(livreID string) (*Livre, error) {

	url := fmt.Sprintf("%s/livres/%s", c.HostURL, livreID)
	url = strings.ReplaceAll(url, `"`, ``)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	body, err := c.doRequest(req)
	if err != nil {
		return nil, err
	}

	livre := Livre{}
	err = json.Unmarshal(body, &livre)
	if err != nil {
		return nil, err
	}

	return &livre, nil
}

// CreateLivre - Create new livre
func (c *Client) CreateLivre(livreItem Livre) (*Livre, error) {

	rb, err := json.Marshal(livreItem)

	jsonVal := strings.ReplaceAll(string(rb), `\"`, ``)

	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/livres", c.HostURL), strings.NewReader(jsonVal))

	if err != nil {
		return nil, err
	}

	body, err := c.doRequest(req)
	if err != nil {
		return nil, err
	}

	livre := Livre{}
	err = json.Unmarshal(body, &livre)
	if err != nil {
		return nil, err
	}

	return &livre, nil
}

// UpdateLivre - Update a livre
func (c *Client) UpdateLivre(livreID string, livreItem Livre) (*Livre, error) {
	rb, err := json.Marshal(livreItem)
	jsonVal := strings.ReplaceAll(string(rb), `\"`, ``)
	if err != nil {
		return nil, err
	}

	log.Println("url Update", fmt.Sprintf("%s/livres/%s", c.HostURL, strings.ReplaceAll(livreID, `"`, ``)))
	log.Println("body Update", strings.NewReader(jsonVal))

	req, err := http.NewRequest("PUT", fmt.Sprintf("%s/livres/%s", c.HostURL, strings.ReplaceAll(livreID, `"`, ``)), strings.NewReader(jsonVal))
	if err != nil {
		return nil, err
	}

	body, err := c.doRequest(req)
	if err != nil {
		return nil, err
	}

	livre := Livre{}
	err = json.Unmarshal(body, &livre)

	if err != nil {
		return nil, err
	}

	return &livre, nil
}

// DeleteLivre - Delete a livre
func (c *Client) DeleteLivre(livreID string) error {

	log.Println("url Delete", fmt.Sprintf("%s/livres/%s", c.HostURL, strings.ReplaceAll(livreID, `"`, ``)))

	req, err := http.NewRequest("DELETE", fmt.Sprintf("%s/livres/%s", c.HostURL, strings.ReplaceAll(livreID, `"`, ``)), nil)
	if err != nil {
		return err
	}

	body, err := c.doRequest(req)
	if err != nil {
		return err
	}

	log.Println("body Delete", body)

	return nil
}
