package provider

import (
	"context"
	clientBeeceptor "terraform-provider-beeceptor/internal/provider/client"
	"time"

	"github.com/hashicorp/terraform-plugin-framework/resource"
	"github.com/hashicorp/terraform-plugin-framework/resource/schema"
	"github.com/hashicorp/terraform-plugin-framework/types"
)

// Ensure the implementation satisfies the expected interfaces.
var (
	_ resource.Resource              = &livreResource{}
	_ resource.ResourceWithConfigure = &livreResource{}
)

// NewLivreResource is a helper function to simplify the provider implementation.
func NewLivreResource() resource.Resource {
	return &livreResource{}
}

// Metadata returns the resource type name.
func (r *livreResource) Metadata(_ context.Context, req resource.MetadataRequest, resp *resource.MetadataResponse) {
	resp.TypeName = "beeceptor_livre"
}

// Create a new resource.
func (r *livreResource) Create(ctx context.Context, req resource.CreateRequest, resp *resource.CreateResponse) {
	// Retrieve values from plan
	var plan livreResourceModel
	diags := req.Plan.Get(ctx, &plan)
	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}

	// Generate API request body from plan
	var items []clientBeeceptor.Livre
	for _, item := range plan.Items {
		items = append(items, clientBeeceptor.Livre{
			Author: item.Author.String(),
			Nom:    item.Nom.String(),
		})
	}

	for index, item := range items {
		livre, err := createLivre(r.client, item)
		if err != nil {
			resp.Diagnostics.AddError(
				"Error creating livre",
				"Could not create livre, unexpected error: "+err.Error(),
			)
			return
		}
		plan.Items[index] = livreItemResourceModel{
			Id:     types.StringValue(livre.Id),
			Author: types.StringValue(livre.Author),
			Nom:    types.StringValue(livre.Nom),
		}
	}

	plan.LastUpdated = types.StringValue(time.Now().Format(time.RFC850))

	// Set state to fully populated data
	diags = resp.State.Set(ctx, plan)
	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}
}

// Read resource information.
func (r *livreResource) Read(ctx context.Context, req resource.ReadRequest, resp *resource.ReadResponse) {

	// Get current state
	var state livreResourceModel
	diags := req.State.Get(ctx, &state)
	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}

	for index, item := range state.Items {
		livre, err := r.client.GetLivre(item.Id.String())
		if err != nil {
			resp.Diagnostics.AddError(
				"Error get Livre",
				"Could not get livre, unexpected error: "+err.Error(),
			)
			return
		}

		// Overwrite items with refreshed state
		state.Items[index] = livreItemResourceModel{
			Id:     types.StringValue(livre.Id),
			Author: types.StringValue(livre.Author),
			Nom:    types.StringValue(livre.Nom),
		}
	}

	// Set refreshed state
	diags = resp.State.Set(ctx, &state)
	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}
}

func (r *livreResource) Update(ctx context.Context, req resource.UpdateRequest, resp *resource.UpdateResponse) {
	// Retrieve values from plan
	var plan livreResourceModel
	var state livreResourceModel
	req.Plan.Get(ctx, &plan)
	diags := req.State.Get(ctx, &state)
	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}

	var numberLivreInState = len(state.Items)
	var numberLivreInPlan = len(plan.Items)

	// Generate API request body from plan
	for index, item := range plan.Items {

		if index+1 <= numberLivreInState {
			livre, err := updateLivre(r.client, state.Items[index].Id.String(), item)
			if err != nil {
				resp.Diagnostics.AddError(
					"Error Updating livre",
					"Could not update livre, unexpected error: "+err.Error(),
				)
				return
			}
			state.Items[index] = livreItemResourceModel{
				Id:     types.StringValue(livre.Id),
				Author: types.StringValue(livre.Author),
				Nom:    types.StringValue(livre.Nom),
			}
		} else {
			livre, err := createLivre(r.client, clientBeeceptor.Livre{
				Author: item.Author.String(),
				Nom:    item.Nom.String(),
			})
			if err != nil {
				resp.Diagnostics.AddError(
					"Error creating livre",
					"Could not create livre, unexpected error: "+err.Error(),
				)
				return
			}
			state.Items = append(state.Items, livreItemResourceModel{
				Id:     types.StringValue(livre.Id),
				Author: types.StringValue(livre.Author),
				Nom:    types.StringValue(livre.Nom),
			})
		}
	}

	//On supprime ceux qui sont dans le state mais pas dans le plan
	if numberLivreInPlan < numberLivreInState {
		for index, item := range state.Items {
			if index+1 > numberLivreInPlan {
				err := deleteLivre(r.client, item.Id.String())
				if err != nil {
					resp.Diagnostics.AddError(
						"Error Deleting Livre",
						"Could not delete livre, unexpected error: "+err.Error(),
					)
					return
				}
				state.Items = remove(state.Items, index)
			}
		}
	}

	state.LastUpdated = types.StringValue(time.Now().Format(time.RFC850))

	diags = resp.State.Set(ctx, &state)
	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}
}

// Delete deletes the resource and removes the Terraform state on success.
func (r *livreResource) Delete(ctx context.Context, req resource.DeleteRequest, resp *resource.DeleteResponse) {
	// Retrieve values from state
	var state livreResourceModel
	diags := req.State.Get(ctx, &state)
	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}

	for _, item := range state.Items {
		err := deleteLivre(r.client, item.Id.String())
		if err != nil {
			resp.Diagnostics.AddError(
				"Error Deleting Livre",
				"Could not delete livre, unexpected error: "+err.Error(),
			)
			return
		}
	}
}

// Configure adds the provider configured client to the resource.
func (r *livreResource) Configure(_ context.Context, req resource.ConfigureRequest, resp *resource.ConfigureResponse) {
	if req.ProviderData == nil {
		return
	}

	client := req.ProviderData.(*clientBeeceptor.Client)
	r.client = client
}

// Schema defines the schema for the resource.
func (r *livreResource) Schema(_ context.Context, _ resource.SchemaRequest, resp *resource.SchemaResponse) {
	resp.Schema = schema.Schema{
		Attributes: map[string]schema.Attribute{
			"last_updated": schema.StringAttribute{
				Computed: true,
			},
			"items": schema.ListNestedAttribute{
				Required: true,
				NestedObject: schema.NestedAttributeObject{
					Attributes: map[string]schema.Attribute{
						"id": schema.StringAttribute{
							Computed: true,
						},
						"author": schema.StringAttribute{
							Required: true,
						},
						"nom": schema.StringAttribute{
							Required: true,
						},
					},
				},
			},
		},
	}
}

// livreResource is the resource implementation.
type livreResource struct {
	client *clientBeeceptor.Client
}

// livreResourceModel maps the resource schema data.
type livreResourceModel struct {
	Items       []livreItemResourceModel `tfsdk:"items"`
	LastUpdated types.String             `tfsdk:"last_updated"`
}

type livreItemResourceModel struct {
	Id     types.String `tfsdk:"id"`
	Author types.String `tfsdk:"author"`
	Nom    types.String `tfsdk:"nom"`
}

func remove[T comparable](slice []T, s int) []T {
	return append(slice[:s], slice[s+1:]...)
}

func deleteLivre(client *clientBeeceptor.Client, id string) error {
	return client.DeleteLivre(id)
}

func createLivre(client *clientBeeceptor.Client, item clientBeeceptor.Livre) (*clientBeeceptor.Livre, error) {
	return client.CreateLivre(item)
}

func updateLivre(client *clientBeeceptor.Client, id string, item livreItemResourceModel) (*clientBeeceptor.Livre, error) {
	return client.UpdateLivre(id, clientBeeceptor.Livre{
		Author: item.Author.String(),
		Nom:    item.Nom.String(),
	})
}
