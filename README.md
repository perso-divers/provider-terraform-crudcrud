# Terraform Provider for [CRUD API Beeceptor ](https://beeceptor.com/crud-api/)

This project is an example of terraform provider.

I personally wanted to discover how a terraform provider works.

I created this provider based on an imaginary CRUD Rest Api maded by Beeceptor. Beeceptor is an easy tool to creat end-points and simulate APIs.

## Requirements

- [Terraform](https://developer.hashicorp.com/terraform/downloads) >= 1.0
- [Go](https://golang.org/doc/install) >= 1.21

## Install

Install the package

```shell
go install
```

Modify your personal file .terraformrc located in ~

```
provider_installation {

  dev_overrides {
      "hashicorp.com/edu/beeceptor" = "/home/{YOU}/go/bin"
  }

  # For all other providers, install them directly from their origin provider
  # registries as normal. If you omit this, Terraform will _only_ use
  # the dev_overrides block, and so no other providers will be available.
  direct {}
}
```

## Using the provider

### Set up

First of all, you need to describe the provider and give a specific database id.

```terraform
terraform {
  required_providers {
    beeceptor = {
      source = "hashicorp.com/edu/beeceptor"
    }
  }
}

provider "beeceptor" {
  database = "cacda518f456a9fdc195"
}
```

### Use the resource

This provider only provide one resource named "beeceptor_livre".
This resource makes only one parameters `items` which is an array composed of object composed of two keys `author` and `nom`.

```terraform
resource "beeceptor_livre" "livres" {
  items = [
    {
      author = "Author1"
      nom = "Nom de livre 1"
    },
        {
      author = "Author2"
      nom = "Nom de livre 2"
    }
  ]
}
```